from app.functions import add


def test_add():
    assert add(2, 5) == 7

def test_add_three_numbers():
    assert add(4, 0, 3) == 7